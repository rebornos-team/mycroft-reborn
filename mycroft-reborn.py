# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Mycroft Reborn")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Depending on your setup, Mycroft may not work properly").show()

workingDirectory = os.path.dirname(os.path.realpath(__file__))
print(workingDirectory)

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Start Mycroft Service
    def onStart(self, button):
        os.system("python3 " + workingDirectory + '/mycroftpassword.py & disown')
        while not os.path.exists("/home/keegan/.mycroftpassword.txt"):
            time.sleep(1)
        with open("/home/keegan/.mycroftpassword.txt") as f:
            firstLine = f.readline()
        from gi.repository import Notify
        Notify.init("Mycroft Reborn")
        os.system("echo '" + firstLine + "' | sudo -S echo")
        os.system("$HOME/mycroft-core/start-mycroft.sh all")
        os.system("rm -f $HOME/.mycroftpassword.txt")
        Notify.Notification.new("Mycroft is ready for you").show()

# Stop Mycroft Service
    def onStop(self, button):
        os.system(workingDirectory + '/reborn-mycroft Stop')
        from gi.repository import Notify
        Notify.init("Mycroft Reborn")
        Notify.Notification.new("Mycroft has been terminated").show()

# Build / Update Mycroft
    def onBuild(self, button):
        os.system('xterm -e ' + workingDirectory + '/reborn-mycroft Build')
        from gi.repository import Notify
        Notify.init("Mycroft Reborn")
        Notify.Notification.new("Mycroft is now built and ready to go!").show()

# Type queries for Mycroft
    def onEnter(self, pkgtxt):
        enteredText = pkgtxt.get_text()
        from gi.repository import Notify
        Notify.init("Mycroft Reborn")
        Notify.Notification.new(enteredText).show()
        os.system("$(echo " + enteredText + " | /home/$USER/mycroft-core/start-mycroft.sh cli) &")

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/mycroft-reborn.glade")
builder.connect_signals(Handler())

window1 = builder.get_object("mycroftWindow")
window1.show_all()

Gtk.main()

Notify.uninit()
