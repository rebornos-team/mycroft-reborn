# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Mycroft Reborn")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Depending on your setup, Mycroft may not work properly").show()

workingDirectory = os.path.dirname(os.path.realpath(__file__))
print(workingDirectory)

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Start Mycroft Service
    def myPassword(self, text):
        global enteredText2
        enteredText2 = text.get_text()
        os.system("echo " + enteredText2 + " >/$HOME/.mycroftpassword.txt")
        Gtk.main_quit();

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/mycroft-reborn.glade")
builder.connect_signals(Handler())

window2 = builder.get_object("mycroftPassword")
window2.show_all()

Gtk.main()

Notify.uninit()
